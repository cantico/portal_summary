<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


function portal_summary_translate($str, $str_plurals = null, $number = null)
{

    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('portal_summary');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}




/**
 * @return Func_Widgets
 */
function portal_summary_Widgets()
{
    return bab_Functionality::get('Widgets');
}






/**
 *
 * @param unknown_type $blockTitle
 * @param unknown_type $nbLines
 * @param unknown_type $hideTypes
 * @param unknown_type $category
 * @param unknown_type $folder
 * @return Widget_VBoxLayout
 */
function portal_summary_displayRecentActivity($blockTitle = null, $nbLines = null, $hideTypes = null, $category = null, $folder = null, $excludeTopics = null)
{
    $W = portal_summary_Widgets();

    $box = $W->VBoxItems();



    if (!isset($blockTitle)) {
        $blockTitle = '';
    }

    $recentElements = array();

    $nbLines = (int)$nbLines;
    if ($nbLines < 0) {
        $nbLines = 0;
    } elseif ($nbLines > 50) {
        $nbLines = 50;
    }

    include_once $GLOBALS['babInstallPath'].'utilit/ovmlapi.php';

    $OvmlApi = new bab_ovmlAPI();

// 	if (((!isset($category)) || empty($category)) && ((!isset($folder)) || empty($folder))) {
// 		$displayAll = true;
// 	} else {
// 		$displayAll = false;
// 	}

    $displayFiles = (!isset($hideTypes['files']) || $hideTypes['files'] == false);
    $displayArticles = (!isset($hideTypes['articles']) || $hideTypes['articles'] == false);


    if ($displayFiles) { //$displayAll || isset($folder) && !empty($folder))) {

        $params = array(
            'last' => $nbLines
        );
        if (isset($folder)) {
            $pathElements = explode('/', $folder);
            array_shift($pathElements);
            $folder = implode('/', $pathElements);
            $params['path'] = $folder;
        }
        $container = $OvmlApi->getContainer(
            'RecentFiles',
            $params
        );


        $element = new stdClass();
        while ($container->getnext($element)) {
            $recentFile = array(
                'type' => 'file_uploaded',
                'sortkey' => $element->FileDate,
                'FileId' => $element->FileId,
                'FileName' => $element->FileName,
                'FilePath' => $element->FilePath,
                'FileDescription' => $element->FileDescription,
                'FileUrl' => $element->FileUrl,
                'FilePopupUrl' => $element->FilePopupUrl,
                'FileUrlGet' => $element->FileUrlGet,
                'FileAuthor' => $element->FileAuthor,
                'FileModifiedBy' => $element->FileModifiedBy,
                'FileDate' => $element->FileDate,
            );
            $recentElements[] = $recentFile;
        }

    }

    if ($displayArticles) { //$displayAll || isset($category) && !empty($category))) {

        $params = array(
            'last' => $nbLines
        );
        if (isset($category)) {
            $params['categoryid'] = $category;
        }
        if (isset($excludeTopics)) {
            $params['excludetopicid'] = $excludeTopics;
        }
        $container = $OvmlApi->getContainer(
            'RecentArticles',
            $params
        );



        $element = new stdClass();
        while ($container->getnext($element)) {
            $recentFile = array(
                'sortkey' => $element->ArticleDateModification,
                'ArticleId' => $element->ArticleId,
                'ArticleTitle' => $element->ArticleTitle,
                'ArticleHead' => $element->ArticleHead,
                'ArticleBody' => $element->ArticleBody,
                'ArticleModifiedBy' => $element->ArticleModifiedBy,
                'ArticleUrl' => $element->ArticleUrl,
                'ArticlePopupUrl' => $element->ArticlePopupUrl,
                'ArticleTopicId' => $element->ArticleTopicId,
                'ArticleDelegationId' => $element->ArticleDelegationId,
                'ArticleFiles' => $element->ArticleFiles,
                'ArticleAverageRating' => $element->ArticleAverageRating,
                'ArticleNbRatings' => $element->ArticleNbRatings,
                'ArticleLanguage' => $element->ArticleLanguage,
                'ArticleEditUrl' => $element->ArticleEditUrl,
                'ArticleEditName' => $element->ArticleEditName,
                'ArticleAuthor' => $element->ArticleAuthor,
                'ArticleDate' => $element->ArticleDate,
                'ArticleDateModification' => $element->ArticleDateModification,
                'ArticleDatePublication' => $element->ArticleDatePublication,
                'ArticleDateCreation' => $element->ArticleDateCreation,
            );

            if ($element->ArticleDateCreation == $element->ArticleDateModification) {
                $recentFile['type'] = 'article_published';
            } else {
                $recentFile['type'] = 'article_modified';
            }

            $recentElements[] = $recentFile;
        }

    }


    bab_Sort::asort($recentElements, 'sortkey');
    $recentElements = array_reverse($recentElements);

    $recentElements = array_slice($recentElements, 0, $nbLines);

    $addon = bab_getAddonInfosInstance('portal_summary');


    $html = '<span id="portal_summary_recent_activity"></span>';



    foreach ($recentElements as $recentElement) {

        $html .= '<!-- ' . $addon->getRelativePath() . $recentElement['type'] . '.html -->';
        $html .= bab_printOvmlTemplate(
            $addon->getRelativePath() . $recentElement['type'] . '.html',
            $recentElement
        );

    }

    $box->addItem($W->Html($html));

    $box = $W->VBoxItems(
        $W->Html('<h1 class="bloc_title">' . bab_toHtml($blockTitle) .'</h1>'),
        $box->setSizePolicy('bloc_content')
    );

    return $box;
}
