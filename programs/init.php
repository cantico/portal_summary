<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


include_once dirname(__FILE__)."/functions.php";


function portal_summary_upgrade($version_base,$version_ini)
{

    global $babBody;
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';

    $addon = bab_getAddonInfosInstance('portal_summary');

    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('portal_summary');
        $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'portal_summary_onBeforeSiteMapCreated', 'init.php');
        $addon->addEventListener('bab_eventBeforePageCreated', 'portal_summary_onBeforePageCreated', 'init.php');
        @bab_functionality::includefile('PortletBackend');
        if (class_exists('Func_PortletBackend')) {
            $addon->registerFunctionality('Func_PortletBackend_PortalSummary', 'portletbackend.class.php');
        }
    } else {
        bab_addEventListener('bab_eventBeforeSiteMapCreated'			,'portal_summary_onBeforeSiteMapCreated'	,'addons/portal_summary/init.php'		, 'portal_summary');
        bab_addEventListener('bab_eventBeforePageCreated'				,'portal_summary_onBeforePageCreated'		,'addons/portal_summary/init.php'		, 'portal_summary');

        $addonPhpPath = $addon->getPhpPath();
        @bab_functionality::includefile('PortletBackend');
        if (class_exists('Func_PortletBackend')) {
            $functionalities = new bab_functionalities();
            $functionalities->registerClass('Func_PortletBackend_PortalSummary', $addonPhpPath . 'portletbackend.class.php');
        }
    }

    portal_summary_translate('has uploaded the document');
    portal_summary_translate('has updated the article');
    portal_summary_translate('has published the article');
    portal_summary_translate('on');

    return true;
}



function portal_summary_onDeleteAddon()
{

    // detach events
    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('portal_summary');
        $addon = bab_getAddonInfosInstance('portal_summary');
        $addon->unregisterFunctionality('Func_PortletBackend_PortalSummary');
    } else {
        include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
        include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        bab_removeEventListener('bab_eventBeforeSiteMapCreated'		,'portal_summary_onBeforeSiteMapCreated'  ,'addons/portal_summary/init.php');
        bab_removeEventListener('bab_eventBeforePageCreated'		,'portal_summary_onBeforePageCreated'	  ,'addons/portal_summary/init.php');

        $func = new bab_functionalities;
        $func->unregisterClass('Func_PortletBackend_PortalSummary');
    }

    return true;
}


function portal_summary_onBeforeSiteMapCreated($event)
{
    $delegations = bab_getUserVisiblesDelegations();

    foreach($delegations as $id_delegation => $deleg) {

        $core_prefix = false === $deleg['id'] ? 'bab' : 'babDG'.$deleg['id'];
        $addon_prefix = false === $deleg['id'] ? 'portalSummary' : 'portalSummaryDG'.$deleg['id'];

        $link = $event->createItem($addon_prefix.'Home');
        $link->setLabel(portal_summary_translate('Summary'));
        $link->setDescription(portal_summary_translate('Last published items'));
        $link->setLink('?tg=addon/portal_summary/main');
        $link->setPosition(array('root', $id_delegation, $core_prefix.'User', $core_prefix.'UserSection'));
        $link->addIconClassname('apps-summary');

        $event->addFunction($link);
    }
}




function portal_summary_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
    $babBody = bab_getBody();
    $addon = bab_getAddonInfosInstance('portal_summary');

    $babBody->addStyleSheet($addon->getStylePath().'portal_summary.css');
}