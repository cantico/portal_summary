; <?php/*

[general]
name                          ="portal_summary"
version                       ="0.2.8"
addon_type			          ="EXTENSION"
encoding                      ="UTF-8"
mysql_character_set_database  ="utf8,latin1"
description                   ="Portal summary, new items from articles, comments, forum posts, files, calendar events"
description.fr                ="Module qui résume dans une page la liste des événements récents sur le portail"
long_description.fr           ="README.md"
delete                        ="1"
addon_access_control          ="0"
author                        ="Cantico"
ov_version                    ="7.1.0"
php_version                   ="5.1.0"
mysql_version                 ="4.1.0"

icon                          ="summary.png"
tags                          ="extension,recommend,intranet"

[addons]
widgets				="1.0.29"
portlets			="0.17"
LibTranslate		=">=1.12.0rc3.01"
;*/?>