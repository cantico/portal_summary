<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';



class portal_summary_PortletDefinition_Articles implements portlet_PortletDefinitionInterface
{

    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;


    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('portal_summary');
    }


    public function getId()
    {
        return 'Articles';
    }

    public function getName()
    {
        return portal_summary_translate('Articles');
    }


    public function getDescription()
    {
        return portal_summary_translate('List of articles with options to select.');
    }


    public function getPortlet()
    {
        return new portal_summary_Portlet_Articles();
    }



    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        $preferenceFields = array();
/*
        $preferenceFields[] = array(
            'type' => 'string',
            'label' => portal_summary_translate('Block title'),
            'name' => 'blockTitle'
        );*/

        $preferenceFields[] = array(
            'type' => 'int',
            'label' => portal_summary_translate('Number of articles to display'),
            'name' => 'nbArticles',
            'default' => 5
        );

        $preferenceFields[] = array(
            'type' => 'idTopic',
            'label' => portal_summary_translate('Aritcles topic'),
            'name' => 'topicId'
        );

        $preferenceFields[] = array(
            'type' => 'idCategory',
            'label' => portal_summary_translate('Article category'),
            'name' => 'categoryId'
        );

        $preferenceFields[] = array(
            'type' => 'int',
            'label' => portal_summary_translate('Number of chars display'),
            'name' => 'nbChars',
            'default' => 100
        );

        $preferenceFields[] = array(
            'type' => 'boolean',
            'label' => portal_summary_translate('Show artilcle date'),
            'name' => 'date',
            'default' => false
        );

        $preferenceFields[] = array(
            'type' => 'color',
            'label' => portal_summary_translate('Text color'),
            'name' => 'textcolor'
        );

        $preferenceFields[] = array(
            'type' => 'color',
            'label' => portal_summary_translate('Background color'),
            'name' => 'bgcolor'
        );

        $preferenceFields[] = array(
        		'type' => 'int',
        		'label' => portal_summary_translate('Image width'),
        		'name' => 'imgWidth'
        );
        return $preferenceFields;
    }


    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getStylePath() . 'images/summary.png';
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getStylePath() . 'images/summary.png';
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return '';
    }

    public function getConfigurationActions()
    {
        return array();
    }
}





class portal_summary_Portlet_Articles extends Widget_Item implements portlet_PortletInterface
{
    private $portletId = null;

    private $nbArticles = null;
    private $topicId = null;
    private $categoryId = null;
    private $nbChars = 100;
    private $date = null;
    private $textcolor = null;
    private $bgcolor = null;
    private $item = null;
    private $imgWidth = null;



    /**
     */
    public function __construct()
    {
        $W = bab_Widgets();

        $this->item = $W->VBoxItems();
    }


    public function getName()
    {
        return get_class($this);
    }


    public function getPortletDefinition()
    {
        return new portal_summary_PortletDefinition_Articles();
    }


    /**
     * receive current user configuration from portlet API
     */
    public function setPreferences(array $configuration)
    {
        foreach ($configuration as $name => $value) {
            $this->setPreference($name, $value);
        }
    }



    public function setPreference($name, $value)
    {
        if ($name === 'nbArticles') {
            $this->nbArticles = $value;
        }

        if ($name === 'topicId') {
            $this->topicId = $value;
        }

        if ($name === 'categoryId') {
            $this->categoryId = $value;
        }

        if ($name === 'nbChars') {
            $this->nbChars = $value;
        }

        if ($name === 'date') {
            $this->date = $value;
        }

        if ($name === 'textcolor') {
            $this->textcolor = $value;
        }

        if ($name === 'bgcolor') {
            $this->bgcolor = $value;
        }

        if ($name === 'imgWidth') {
            $this->imgWidth = $value.'px';
        }
    }


    public function setPortletId($id)
    {
        $this->portletId = $id;
    }





    /**
     * @param Widget_Canvas	$canvas
     * @ignore
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = bab_Widgets();

        $param = '';

        if ($this->nbArticles) {
            $param .= ' last="' . $this->nbArticles . '"';
        }

        if ($this->categoryId) {
            $this->topicId = null;

            $param .= ' categoryid="' . $this->categoryId . '"';
        }

        $ovml = '';

        if ($this->topicId) {
            $ovml .= '<span class="bab-articletopic-' . $this->topicId . '"></span>';
        }

        $ovml .= '<div class="portal-content portal-articles-list">';

        if ($this->topicId) {
            $ovml .= '<OCArticleTopic topicid="' . $this->topicId . '">';

            $param .= ' topicid="' . $this->topicId . '"';
        }

        $style = '';
        $styleImg = '';
        if ($this->textcolor) {
            $style .= 'color: #' . $this->textcolor . '; ';
        }
        if ($this->bgcolor) {
            $style .= 'background-color: #' . $this->bgcolor . ';';
        }
        if ($this->imgWidth) {
            $styleImg .= 'width: ' . $this->imgWidth . ';';
        }

        $ovml.= '<OCRecentArticles '.$param.'>';
        $ovml.= '<div style="'.$style.'" class="portal-article-item bab-article-<OVArticleId>">';
        $ovml.= '<a style="'.$style.'" class="portal-item-link" href="<OVArticleUrl>">';
        $ovml.= '<OCIfNotEqual expr1="<OVImageUrl>" expr2="">';
        	$ovml.= '<img style="'.$styleImg.'" src="<OVImageUrl>">';
        $ovml.= '</OCIfNotEqual>';
        $ovml.= '<h2 class="title-portlet-article"><OVArticleTitle></h2>';
        if($this->date){
            $ovml.= '&nbsp;-&nbsp;<OVArticleDate  date="%D %j %M %H:%i">';
        }
        $ovml.= '<p><OVArticleHead  striptags="1" strlen="'.$this->nbChars.', ..." htmlentities="1"></p>';
        $ovml.= '</a>';
        $ovml.= '</div>';
        $ovml.= '</OCRecentArticles>';
        //$ovml.= '<OCIfNotEqual expr1="<OVTopicSubmitUrl>" expr2="">';
        //$ovml.= '<div class="ws_submitaction"><a href="<OVTopicSubmitUrl>" target="_blank">Submit an article in "<OVTopicName>"</a></div>';
        //$ovml.= '</OCIfNotEqual>

        if ($this->topicId) {
            $ovml.= '</OCArticleTopic>';
        }

        $ovml.= '</div>';


        $layout = $W->Html(bab_printOvml($ovml, array()));

        $display = $layout->display($canvas);

        return $display;
    }
}

