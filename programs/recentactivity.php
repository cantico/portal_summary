<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';





$idx = bab_rp('idx');

switch ($idx) {

	case 'displayList':
		
		

		$fullpage = bab_rp('fullpage', null);
		$blockTitle = bab_rp('blockTitle', null);
		if (!is_string($blockTitle)) {
			$blockTitle = null;
		} else {
			if (mb_strlen($blockTitle) > 100) {
				$blockTitle = mb_substr($blockTitle, 0, 100);
			}
		}

		$nbLines = bab_rp('nbLines', 6);
		$nbLines = (int)$nbLines;
		if ($nbLines < 0) {
			$nbLines = 0;
		} elseif ($nbLines > 50) {
			$nbLines = 50;
		}

		$category = bab_rp('category', null);
		$folder = bab_rp('folder', null);
		$excludeTopics = bab_rp('excludeTopics', null);
		
		$hideTypes = bab_rp('hideTypes', null);
		if (!is_array($hideTypes)) {
			$hideTypes = null;
		} else {

		}

		$W = portal_summary_Widgets();
		$htmlCanvas = $W->HtmlCanvas();

		$box = portal_summary_displayRecentActivity($blockTitle, $nbLines, $hideTypes, $category, $folder, $excludeTopics);
		
		$html = $box->display($htmlCanvas);
		


		if (!isset($fullpage)) {
			
			$urlfullpage = '?tg=addon/portal_summary/recentactivity&idx=displayList&nbLines=50&fullpage=1';
			if ($category) {
				$urlfullpage .= '&category=' . urlencode($category);
			}
			if ($folder) {
				$urlfullpage .= '&folder=' . urlencode($folder);
			}
			
			$html .= '
				<div class="bloc_nav">
				<ul>
				<li class="active plus"><a href="' . $urlfullpage . '">&nbsp;</a></li>
				</ul>
				</div>';
			die($html);
		}
		$babBody->setTitle(portal_summary_translate('Recent activity'));
		$babBody->babecho($html);
		

		return;
		
}

die();







