<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


bab_Functionality::includefile('PortletBackend');


require_once dirname(__FILE__) . '/portletrecentactivity.class.php';
require_once dirname(__FILE__) . '/portletarticles.class.php';

/**
 * PortalSummary Portlet backend
 */
class Func_PortletBackend_PortalSummary extends Func_PortletBackend
{
    public function getDescription()
    {
        return portal_summary_translate('Portal Summary Portlets');
    }


    public function select($category = null)
    {
        return array(
            'RecentActivity' => $this->portlet_RecentActivity(),
            'Articles' => $this->portlet_Articles()
        );
    }


    public function portlet_RecentActivity()
    {
        return new portal_summary_PortletDefinition_RecentActivity();
    }


    public function portlet_Articles()
    {
        return new portal_summary_PortletDefinition_Articles();
    }



    /**
     * (non-PHPdoc)
     * @see Func_PortletBackend::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}



