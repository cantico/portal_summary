<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';



class portal_summary_PortletDefinition_RecentActivity implements portlet_PortletDefinitionInterface
{

	/**
	 * @var bab_addonInfos $addon
	 */
	protected $addon;


	public function __construct()
	{
		$this->addon = bab_getAddonInfosInstance('portal_summary');
	}


	public function getId()
	{
		return 'RecentActivity';
	}

	public function getName()
	{
		return portal_summary_translate('Recent activity');
	}


	public function getDescription()
	{
		return portal_summary_translate('List of recently published articles or files.');
	}


	public function getPortlet()
	{
		return new portal_summary_Portlet_RecentActivity();
	}



	/**
	 * @return array
	 */
	public function getPreferenceFields()
	{
		$preferenceFields = array();

		$preferenceFields[] = array(
			'type' => 'string',
			'label' => portal_summary_translate('Block title'),
			'name' => 'blockTitle'
		);

		$preferenceFields[] = array(
			'type' => 'int',
			'label' => portal_summary_translate('Number of lines to display'),
			'name' => 'nbLines',
			'default' => 10
		);



		$preferenceFields[] = array(
			'type' => 'int',
			'label' => portal_summary_translate('Article category'),
			'name' => 'category'
		);

		$preferenceFields[] = array(
			'type' => 'string',
			'label' => portal_summary_translate('Topics to exclude'),
			'name' => 'excludeTopics'
		);


		$preferenceFields[] = array(
			'type' => 'bab_file',
			'label' => portal_summary_translate('Folder'),
			'name' => 'folder'
		);



		$preferenceFields[] = array(
			'type' => 'boolean',
			'label' => portal_summary_translate('Hide articles'),
			'name' => 'hideArticles'
		);

		$preferenceFields[] = array(
			'type' => 'boolean',
			'label' => portal_summary_translate('Hide files'),
			'name' => 'hideFiles'
		);


		return $preferenceFields;
	}


	/**
	 * Returns the widget rich icon URL.
	 * 128x128 ?
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return $this->addon->getStylePath() . 'images/summary.png';
	}


	/**
	 * Returns the widget icon URL.
	 * 16x16 ?
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return $this->addon->getStylePath() . 'images/summary.png';
	}

	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return '';
	}

	public function getConfigurationActions()
	{
		return array();
	}
}





class portal_summary_Portlet_RecentActivity extends Widget_Item implements portlet_PortletInterface
{
	private $portletId = null;

	private $blockTitle = null;

	private $nbLines = null;

	private $category = null;

	private $folder = null;

	private $excludeTopics = null;

	private $hideTypes = array();

	private $item = null;



	/**
	 */
	public function __construct()
	{
	    $W = bab_Widgets();

		$this->item = $W->VBoxItems();
	}


	public function getName()
	{
		return get_class($this);
	}


	public function getPortletDefinition()
	{
		return new portal_summary_PortletDefinition_RecentActivity();
	}


	/**
	 * receive current user configuration from portlet API
	 */
	public function setPreferences(array $configuration)
	{
		foreach ($configuration as $name => $value) {
			$this->setPreference($name, $value);
		}
	}



	public function setPreference($name, $value)
	{

		if ($name === 'hideArticles') {
			$this->hideTypes['articles'] = $value;
		}
		if ($name === 'hideFiles') {
			$this->hideTypes['files'] = $value;
		}

		if ($name === 'blockTitle') {
			$this->blockTitle = $value;
		}
		if ($name === 'nbLines') {
			$this->nbLines = $value;
		}
		if ($name === 'category') {
			$this->category = $value;
		}
		if ($name === 'folder') {
			$this->folder = $value;
		}
		if ($name === 'excludeTopics') {
			$this->excludeTopics = $value;
		}


	}


	public function setPortletId($id)
	{
		$this->portletId = $id;
	}





	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
	    $W = bab_Widgets();

		$displayRecentActivityActions = $W->Action();
		$displayRecentActivityActions->setMethod(
			'addon/portal_summary/recentactivity',
			'displayList',
			array(
				'blockTitle' => $this->blockTitle,
				'nbLines' => $this->nbLines,
				'hideTypes' => $this->hideTypes,
				'category' => $this->category,
				'folder' => $this->folder,
				'excludeTopics' => $this->excludeTopics
			)
		);
		$delayedItem = $W->DelayedItem(
			$displayRecentActivityActions
		);
		$box = portal_summary_displayRecentActivity($this->blockTitle, $this->nbLines, $this->hideTypes, $this->category, $this->folder, $this->excludeTopics);

		$delayedItem->setPlaceHolderItem($box);
		$delayedItem->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE);


		$display = $delayedItem->display($canvas);

		$urlfullpage = '?tg=addon/portal_summary/recentactivity&idx=displayList&nbLines=50&fullpage=1';
		if ($this->category) {
			$urlfullpage .= '&category=' . urlencode($this->category);
		}
		if ($this->folder) {
			$urlfullpage .= '&folder=' . urlencode($this->folder);
		}

		$display .= '
			<div class="bloc_nav">
			<ul>
			<li class="active plus"><a href="' . $urlfullpage . '">&nbsp;</a></li>
			</ul>
			</div>';


		$display .= '
			<script type="text/javascript">
				var elementId = "portal_summary_recent_activity";
				window.setTimeout(function() { window.babAddonWidgets.reload(document.getElementById(elementId)); }, 20 * 1000); /* After 20s */
				window.setTimeout(function() { window.babAddonWidgets.reload(document.getElementById(elementId)); }, 50 * 1000); /* 30s later */
				window.setTimeout(function() { window.babAddonWidgets.reload(document.getElementById(elementId)); }, 110 * 1000); /* 60s later */
				window.setInterval(function() { window.babAddonWidgets.reload(document.getElementById(elementId)); }, 30 * 60 * 1000); /* Every 1/2 hour */
			</script>
		';

		return $display;

	}

}



